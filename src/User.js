export class User
{
    /**  @protected @type {string} */ name;
    /** @protected @type {number} */ age;

    /**
     * @param {import('../types').TUserConfig} config
     */
    constructor (config)
    {
        this.name = config.name;
        this.age = config.age;
    }

    getAge()
    {
        return this.age;
    }
}